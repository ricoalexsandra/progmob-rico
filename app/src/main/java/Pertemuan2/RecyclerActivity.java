package Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.helloworld.R;

import java.util.ArrayList;
import java.util.List;

import Adapter.MahasiswaRecyclerAdapter;
import Model.Mahasiswa;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView)findViewById(R.id.RvLatihan);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //data
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mhs
        Mahasiswa m2 = new Mahasiswa("Rico Alex", "72180200", "08931435242");
        Mahasiswa m3 = new Mahasiswa("Dito Adriel", "72180201", "084242415343");
        Mahasiswa m4 = new Mahasiswa("Nadia Angel", "72180202", "0852424242");
        Mahasiswa m5 = new Mahasiswa("Fina Febri", "72180203", "0831319912323");
        Mahasiswa m6 = new Mahasiswa("Keren", "72180207", "089999");

        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);
        mahasiswaList.add(m6);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);

    }
}