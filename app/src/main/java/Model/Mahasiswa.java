package Model;

public class Mahasiswa {
    private String nama,nim,telp;

    public Mahasiswa(String nama, String nim, String telp) {
        this.nama = nama;
        this.nim = nim;
        this.telp = telp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }
}
