package com.example.helloworld.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helloworld.CRUD.HapusMhsActivity;
import com.example.helloworld.CRUD.MahasiswaUpdateActivity;
import com.example.helloworld.Model.Mahasiswa;
import com.example.helloworld.R;

import java.util.ArrayList;
import java.util.List;

public class MahasiswaCRUDRecyclerAdapter extends RecyclerView.Adapter<MahasiswaCRUDRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<Mahasiswa> mahasiswaList;

    public MahasiswaCRUDRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaList = new ArrayList<>();
    }

    public MahasiswaCRUDRecyclerAdapter(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
    }

    public List<Mahasiswa> getMahasiswaList() {
        return mahasiswaList;
    }

    public void setMahasiswaList(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview, parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Mahasiswa m = mahasiswaList.get(position);

        holder.txtNama.setText(m.getNama());
        holder.txtNim.setText(m.getNim());
        holder.txtAlamat.setText(m.getAlamat());
        holder.txtEmail.setText(m.getEmail());
        holder.m = m;
    }

    @Override
    public int getItemCount() {
        return mahasiswaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements PopupMenu.OnMenuItemClickListener {

        private TextView txtNama,txtNim, txtAlamat, txtEmail, txtNoTelp;
        private RecyclerView rvGetMhs;
        private ImageView pop_menu;
        Mahasiswa m;
        private static final String TAG = "MyViewHolder";

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.txtNama);
            txtNim = itemView.findViewById(R.id.txtNim);
            txtAlamat = itemView.findViewById(R.id.txtAlamat);
            txtEmail = itemView.findViewById(R.id.txtEmail);
            rvGetMhs = itemView.findViewById(R.id.rvGetMhs);
            pop_menu = itemView.findViewById(R.id.ivPopupMahasiswa);

            pop_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "tesssss" + getAdapterPosition());
                    ShowMenu(v);
                }
            });
        }

        private void ShowMenu(View view){
            PopupMenu popupMenu = new PopupMenu(view.getContext(),view);
            popupMenu.inflate(R.menu.pop_menu);
            popupMenu.setOnMenuItemClickListener(this);
            popupMenu.show();
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()){
                case R.id.popEdit:
                    Intent intentEdit = new Intent(itemView.getContext(), MahasiswaUpdateActivity.class);
                    intentEdit.putExtra("nama", m.getNama());
                    intentEdit.putExtra("nim", m.getNim());
                    intentEdit.putExtra("alamat", m.getAlamat());
                    intentEdit.putExtra("email", m.getEmail());
                    Bundle b = new Bundle();
                    intentEdit.putExtras(b);
                    itemView.getContext().startActivity(intentEdit);
                    return true;

                case R.id.popDelete:
                    Intent intentDel = new Intent(itemView.getContext(), HapusMhsActivity.class);
                    intentDel.putExtra("nim", m.getNim());
                    Bundle del = new Bundle();
                    intentDel.putExtras(del);
                    itemView.getContext().startActivity(intentDel);
                    return true;
                default:
                    return false;
            }
        }
    }
}
