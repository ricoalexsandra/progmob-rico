package com.example.helloworld.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helloworld.Model.Matkul;
import com.example.helloworld.R;
import com.example.helloworld.UTS.MatkulDeleteActivity;
import com.example.helloworld.UTS.MatkulUpdateActivity;

import java.util.ArrayList;
import java.util.List;

public class MatakuliahRecyclerAdapter extends RecyclerView.Adapter<MatakuliahRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<Matkul> matkulList;

    public MatakuliahRecyclerAdapter(Context context) {
        this.context = context;
        matkulList = new ArrayList<>();
    }

    public MatakuliahRecyclerAdapter(List<Matkul> matkulList) {
        this.matkulList = matkulList;
    }

    public List<Matkul> getMatkulList() {
        return matkulList;
    }

    public void setMatkulList(List<Matkul> matkulList) {
        this.matkulList = matkulList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_mk, parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Matkul mk = matkulList.get(position);

        holder.txtId.setText(mk.getId());
        holder.txtKode.setText(mk.getKode());
        holder.txtNama.setText(mk.getNama());
        holder.txtHari.setText(mk.getHari());
        holder.txtSesi.setText(mk.getSesi());
        holder.txtSks.setText(mk.getSks());
        holder.mk = mk;
    }

    @Override
    public int getItemCount() {
        return matkulList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements PopupMenu.OnMenuItemClickListener {

        private TextView txtId, txtNama, txtHari, txtSesi, txtSks, txtKode;
        private RecyclerView rvgetMk;
        private ImageView pop_menu;
        Matkul mk;
        private static final String TAG = "MyViewHolder";

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtId = itemView.findViewById(R.id.txtIdMk);
            txtNama = itemView.findViewById(R.id.txtNamaMk);
            txtHari = itemView.findViewById(R.id.txtHariMk);
            txtSesi = itemView.findViewById(R.id.txtSesiMk);
            txtSks = itemView.findViewById(R.id.txtSksMk);
            txtKode = itemView.findViewById(R.id.txtKodeMk);
            rvgetMk = itemView.findViewById(R.id.rvGetMk);
            pop_menu = itemView.findViewById(R.id.ivPopupMk);

            //Coba Get posisisnya aja
            pop_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Nomer urut " + getAdapterPosition());
                    ShowMenu(v);
                }
            });
        }

        private void ShowMenu(View view){
            PopupMenu popupMenu = new PopupMenu(view.getContext(),view);
            popupMenu.inflate(R.menu.pop_menu);
            popupMenu.setOnMenuItemClickListener(this);
            popupMenu.show();
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()){
                case R.id.popEdit:
                    Intent intentEdit = new Intent(itemView.getContext(), MatkulUpdateActivity.class);
                    intentEdit.putExtra("nama", mk.getNama());
                    intentEdit.putExtra("kode", mk.getKode());
                    intentEdit.putExtra("hari", mk.getHari());
                    intentEdit.putExtra("sesi", mk.getSesi());
                    intentEdit.putExtra("sks", mk.getSks());
                    Bundle edit = new Bundle();
                    intentEdit.putExtras(edit);
                    itemView.getContext().startActivity(intentEdit);
                    return true;

                case R.id.popDelete:
                    Intent intentDel = new Intent(itemView.getContext(), MatkulDeleteActivity.class);
                    intentDel.putExtra("kode", mk.getKode());
                    Bundle del = new Bundle();
                    intentDel.putExtras(del);
                    itemView.getContext().startActivity(intentDel);
                    return true;
                default:
                    return false;
            }
        }
    }
}
