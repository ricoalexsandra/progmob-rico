package com.example.helloworld.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helloworld.Model.Dosen;
import com.example.helloworld.R;
import com.example.helloworld.UTS.DosenDeleteActivity;
import com.example.helloworld.UTS.DosenUpdateActivity;

import java.util.ArrayList;
import java.util.List;

public class DosenRecyclerAdapter extends RecyclerView.Adapter<DosenRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<Dosen> dosenList;

    public DosenRecyclerAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }

    public DosenRecyclerAdapter(List<Dosen> dosenList) {
        this.dosenList = dosenList;
    }

    public List<Dosen> getDosenList() {
        return dosenList;
    }

    public void setDosenListList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_dosen, parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Dosen d = dosenList.get(position);

        holder.txtNama.setText(d.getNama());
        holder.txtNidn.setText(d.getNidn());
        holder.txtAlamat.setText(d.getAlamat());
        holder.txtEmail.setText(d.getEmail());
        holder.txtGelar.setText(d.getGelar());
        holder.d = d;
    }

    @Override
    public int getItemCount() {
        return dosenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements PopupMenu.OnMenuItemClickListener {

        private TextView txtNama,txtNidn, txtAlamat, txtEmail, txtGelar;
        private RecyclerView rvGetDsn;
        private ImageView pop_menu;
        Dosen d;
        private static final String TAG = "MyViewHolder";

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.txtNamaDosen);
            txtNidn = itemView.findViewById(R.id.txtNidnDosen);
            txtAlamat = itemView.findViewById(R.id.txtAlamatDosen);
            txtEmail = itemView.findViewById(R.id.txtEmailDosen);
            txtGelar = itemView.findViewById(R.id.txtGelarDosen);
            rvGetDsn = itemView.findViewById(R.id.rvGetDsn);
            pop_menu = itemView.findViewById(R.id.imageView3);

            pop_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "tesssss" + getAdapterPosition());
                    ShowMenu(v);
                }
            });
        }

        private void ShowMenu(View view){
            PopupMenu popupMenu = new PopupMenu(view.getContext(),view);
            popupMenu.inflate(R.menu.pop_menu);
            popupMenu.setOnMenuItemClickListener(this);
            popupMenu.show();
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()){
                case R.id.popEdit:
                    Intent intentEdit = new Intent(itemView.getContext(), DosenUpdateActivity.class);
                    intentEdit.putExtra("nama", d.getNama());
                    intentEdit.putExtra("nidn", d.getNidn());
                    intentEdit.putExtra("alamat", d.getAlamat());
                    intentEdit.putExtra("email", d.getEmail());
                    intentEdit.putExtra("gelar", d.getGelar());
                    Bundle edit = new Bundle();
                    intentEdit.putExtras(edit);
                    itemView.getContext().startActivity(intentEdit);
                    return true;

                case R.id.popDelete:
                    Intent intentDel = new Intent(itemView.getContext(), DosenDeleteActivity.class);
                    intentDel.putExtra("nidn", d.getNidn());
                    Bundle del = new Bundle();
                    intentDel.putExtras(del);
                    itemView.getContext().startActivity(intentDel);
                    return true;
                default:
                    return false;
            }
        }
    }
}
