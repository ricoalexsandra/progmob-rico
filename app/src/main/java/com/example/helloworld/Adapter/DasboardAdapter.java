package com.example.helloworld.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helloworld.CRUD.MahasiswaGetActivity;
import com.example.helloworld.R;
import com.example.helloworld.UTS.DosenGetActivity;
import com.example.helloworld.UTS.MatkulGetActivity;

import java.util.ArrayList;
import java.util.List;

public class DasboardAdapter extends RecyclerView.Adapter<DasboardAdapter.DasboardView> {

    private Context context;
    List<Integer> ivList = new ArrayList<>();
    List<String> txtData = new ArrayList<>();

    private OnItemClickListener mListener;
    public interface OnItemClickListener {
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public DasboardAdapter(List<Integer> ivList, List<String> txtData) {
        this.ivList = ivList;
        this.txtData = txtData;
    }

    @NonNull
    @Override
    public DasboardView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dasboard,parent,false);
        return new DasboardView(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull DasboardView holder, int position) {
        holder.ivMahasiswa.setImageResource(ivList.get(position));
        holder.txtDataMahasiswa.setText(txtData.get(position));
    }

    @Override
    public int getItemCount() {
        return ivList.size();
    }

    public class DasboardView extends RecyclerView.ViewHolder{

        ImageView ivMahasiswa;
        TextView txtDataMahasiswa;

        public DasboardView(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            ivMahasiswa = (ImageView)itemView.findViewById(R.id.ivMahasiswa);
            txtDataMahasiswa = (TextView)itemView.findViewById(R.id.txtDataMahasiswa);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getAdapterPosition()==0) {
                        Intent intentMhs = new Intent(itemView.getContext(), MahasiswaGetActivity.class);
                        itemView.getContext().startActivity(intentMhs);
                    }else if(getAdapterPosition()==1){
                        Intent intentDosen = new Intent(itemView.getContext(), DosenGetActivity.class);
                        itemView.getContext().startActivity(intentDosen);
                    }else{
                        Intent intentMk = new Intent(itemView.getContext(), MatkulGetActivity.class);
                        itemView.getContext().startActivity(intentMk);
                    }
                }
            });
        }
    }

}
