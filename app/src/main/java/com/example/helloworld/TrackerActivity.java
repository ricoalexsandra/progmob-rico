package com.example.helloworld;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class TrackerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker);

        TextView textProtein = (TextView)findViewById(R.id.txtProtein);
        TextView textTotal = (TextView)findViewById(R.id.txtTotal);
        TextView textGoal = (TextView)findViewById(R.id.txtGoal);
        TextView textNeeded = (TextView)findViewById(R.id.txtNeeded);
        Button btnEnter = (Button)findViewById(R.id.btnEnter);
        Button btnReset = (Button)findViewById(R.id.btnReset);

        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }
}