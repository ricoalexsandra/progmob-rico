package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.helloworld.Pertemuan2.CardViewTestActivity;
import com.example.helloworld.Pertemuan2.ListActivity;
import com.example.helloworld.Pertemuan2.RecyclerActivity;
import com.example.helloworld.Pertemuan4.DebuggingActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //textView
        final TextView mainText = (TextView)findViewById(R.id.mainTxt);

        //EditView
        final EditText editText = (EditText)findViewById(R.id.editText);

        //Button
        Button btnReplace = (Button)findViewById(R.id.btnReplace);
        Button btnHelp = (Button)findViewById(R.id.btnHelp);
        Button btnTracker = (Button)findViewById(R.id.btnTracker);

        //Button Pertemuan 2
        Button btnRecycler = (Button)findViewById(R.id.btnRecycler);
        Button btnList = (Button)findViewById(R.id.btnList);
        Button btnCard = (Button)findViewById(R.id.btnCard);

        //Button Pertemuan 4
        Button btnDebug = (Button)findViewById(R.id.btnDebug);

        btnReplace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainText.setText(editText.getText().toString());
            }
        });

        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentHelp = new Intent(MainActivity.this, HelpActivity.class);
                Bundle b = new Bundle();
                b.putString("help_string",editText.getText().toString());
                intentHelp.putExtras(b);
                startActivity(intentHelp);
            }
        });

        btnTracker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTracker = new Intent(MainActivity.this,TrackerActivity.class);
                startActivity(intentTracker);
            }
        });

        btnRecycler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRecycler = new Intent(MainActivity.this, RecyclerActivity.class);
                startActivity(intentRecycler);
            }
        });

        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentList = new Intent(MainActivity.this, ListActivity.class);
                startActivity(intentList);
            }
        });

        btnCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCard = new Intent(MainActivity.this, CardViewTestActivity.class);
                startActivity(intentCard);
            }
        });

        btnDebug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent IntentDebug = new Intent(MainActivity.this, DebuggingActivity.class);
                startActivity(IntentDebug);
            }
        });

    }
}