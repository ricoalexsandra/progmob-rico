package com.example.helloworld.CRUD;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.helloworld.Model.DefaultResult;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbahActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah);

        EditText ubahNim = (EditText)findViewById(R.id.ubahNim);
        EditText ubahNama = (EditText)findViewById(R.id.ubahNama);
        EditText ubahAlamat = (EditText)findViewById(R.id.ubahAlamat);
        EditText ubahEmail = (EditText)findViewById(R.id.ubahEmail);
        Button btnUbah = (Button)findViewById(R.id.btnUbah);

        pd = new ProgressDialog(UbahActivity.this);

        Bundle bUbah = getIntent().getExtras();
        String textNim = bUbah.getString("nim");
        ubahNim.setText(textNim);

        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                        ubahNim.getText().toString(),
                        "skip",
                        "fafa",
                        "fafa",
                        "fsfs",
                        "72180198"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UbahActivity.this, "Data berhasil di Edit", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UbahActivity.this, "Data tidak berhasil di Edit", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }
}