package com.example.helloworld.CRUD;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.helloworld.Model.DefaultResult;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaAddActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_add);

        //Toolbar
        SharedPreferences pref = MahasiswaAddActivity.this.getSharedPreferences("mhs_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        androidx.appcompat.widget.Toolbar mytoolbar = (Toolbar)findViewById(R.id.toolbarAddMhs);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setTitle("TAMBAH DATA MAHASISWA");
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        EditText addNama = (EditText)findViewById(R.id.addNama);
        EditText addNim = (EditText)findViewById(R.id.addNim);
        EditText addAlamat = (EditText)findViewById(R.id.addAlamat);
        EditText addEmail = (EditText)findViewById(R.id.addEmail);
        Button btnSimpan = (Button)findViewById(R.id.btnSimpan);

        pd = new ProgressDialog(MahasiswaAddActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                        addNama.getText().toString(),
                        addNim.getText().toString(),
                        addAlamat.getText().toString(),
                        addEmail.getText().toString(),
                        "Kosongkan saja ",
                        "72180198"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaAddActivity.this, "Data berhasil masuk", Toast.LENGTH_LONG).show();
                        Intent go = new Intent(MahasiswaAddActivity.this, MahasiswaGetActivity.class);
                        startActivity(go);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaAddActivity.this, "Data tidak berhasil di input", Toast.LENGTH_LONG).show();
                        Intent go = new Intent(MahasiswaAddActivity.this, MahasiswaGetActivity.class);
                        startActivity(go);
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        return true;
    }
}