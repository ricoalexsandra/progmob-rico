package com.example.helloworld.CRUD;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.helloworld.Model.DefaultResult;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaUpdateActivity extends AppCompatActivity {

    private static final String TAG = "MahasiswaUpdateActivity";
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);

        //Toolbar
        SharedPreferences pref = MahasiswaUpdateActivity.this.getSharedPreferences("mhs_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        androidx.appcompat.widget.Toolbar mytoolbar = (Toolbar)findViewById(R.id.toolbarUpdateMhs);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setTitle("UPDATE DATA MAHASISWA");
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        EditText editNama = (EditText)findViewById(R.id.editNama);
        EditText editNim = (EditText)findViewById(R.id.editNimBaru);
        EditText editNimCari = (EditText)findViewById(R.id.editNimCari);
        EditText editAlamat = (EditText)findViewById(R.id.editAlamat);
        EditText editEmail = (EditText)findViewById(R.id.editEmail);
        Button btnEdit = (Button)findViewById(R.id.btnEdit);
        Button btnDel = (Button)findViewById(R.id.btnDel);

        pd = new ProgressDialog(MahasiswaUpdateActivity.this);

        Intent data = getIntent();
        editNama.setText(data.getStringExtra("nama"));
        editNim.setText(data.getStringExtra("nim"));
        editNimCari.setText(data.getStringExtra("nim"));
        editAlamat.setText(data.getStringExtra("alamat"));
        editEmail.setText(data.getStringExtra("email"));

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_mhs(
                        editNama.getText().toString(),
                        editNim.getText().toString(),
                        editNimCari.getText().toString(),
                        editAlamat.getText().toString(),
                        editEmail.getText().toString(),
                        "auto",
                        "72180198"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "Data berhasil di Update", Toast.LENGTH_LONG).show();
                        Intent go = new Intent(MahasiswaUpdateActivity.this, MahasiswaGetActivity.class);
                        startActivity(go);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "Data tidak berhasil di Update", Toast.LENGTH_LONG).show();
                        Intent go = new Intent(MahasiswaUpdateActivity.this, MahasiswaGetActivity.class);
                        startActivity(go);
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        return true;
    }
}