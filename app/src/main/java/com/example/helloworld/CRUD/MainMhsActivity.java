package com.example.helloworld.CRUD;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.helloworld.R;

public class MainMhsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mhs);

        Button btnGetMhs = (Button)findViewById(R.id.btnGetMhs);
        Button btnAddMhs = (Button)findViewById(R.id.btnAddMhs);

        btnGetMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent IntentGetMhs = new Intent(MainMhsActivity.this, MahasiswaGetActivity.class);
                startActivity(IntentGetMhs);
            }
        });

        btnAddMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent IntentAddMhs = new Intent(MainMhsActivity.this, MahasiswaAddActivity.class);
                startActivity(IntentAddMhs);
            }
        });

        /*btnDelMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent IntentDelMhs = new Intent(MainMhsActivity.this, HapusMhsActivity.class);
                startActivity(IntentDelMhs);
            }
        });*/

    }
}