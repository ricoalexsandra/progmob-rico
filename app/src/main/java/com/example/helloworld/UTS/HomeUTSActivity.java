package com.example.helloworld.UTS;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.helloworld.R;

public class HomeUTSActivity extends AppCompatActivity {

    private static int SPLASH_SCREEN = 3000;
    Animation top,bot;
    ImageView logo;
    TextView nama, nim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(HomeUTSActivity.this, "Welcome", Toast.LENGTH_SHORT).show();

/*      session = PreferenceManager.getDefaultSharedPreferences(HomeUTSActivity.this);
        if(!session.getString("nimnik", "").isEmpty() && !session.getString("nama", "").isEmpty()) {
            finish();
            startActivity(new Intent(HomeUTSActivity.this, DasboardUTSActivity.class));
            return;
        }*/

        setContentView(R.layout.activity_home_u_t_s);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        logo = findViewById(R.id.logo);
        nama = findViewById(R.id.nama);
        nim = findViewById(R.id.nim);
        top = AnimationUtils.loadAnimation(this,R.anim.top_anim);
        bot = AnimationUtils.loadAnimation(this,R.anim.bott_anim);

        logo.setAnimation(top);
        nama.setAnimation(bot);
        nim.setAnimation(bot);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent goMain = new Intent(HomeUTSActivity.this, MainUTSActivity.class);
                startActivity(goMain);
                finish();
            }
        },SPLASH_SCREEN);

    }
}
