package com.example.helloworld.UTS;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helloworld.Adapter.DasboardAdapter;
import com.example.helloworld.R;

import java.util.ArrayList;
import java.util.List;

public class DasboardUTSActivity extends AppCompatActivity {

    Toolbar tbDasboard;
    RecyclerView rvDasboard;
    DasboardAdapter dasboardAdapter;
    List<Integer> ivList = new ArrayList<>();
    List<String> txtData = new ArrayList<>();
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dasboard_u_t_s);

        initialize();

        //Toolbar
        SharedPreferences pref = DasboardUTSActivity.this.getSharedPreferences("dasboard_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        androidx.appcompat.widget.Toolbar mytoolbar = (Toolbar)findViewById(R.id.toolbarDasboard);
        setSupportActionBar(mytoolbar);

        session = PreferenceManager.getDefaultSharedPreferences(DasboardUTSActivity.this);
        if(!session.getString("nimnik", "nimnik").isEmpty() && !session.getString("nama", "nama").isEmpty()) {
            return;
        }
    }

    private void initialize(){
        tbDasboard = (Toolbar)findViewById(R.id.toolbarDasboard);
        rvDasboard = (RecyclerView)findViewById(R.id.rvDasboard);

        //SetTitle Toolbar
        Intent data = getIntent();
        tbDasboard.setTitle("Hai - " + data.getStringExtra("nama"));


        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        rvDasboard.setLayoutManager(gridLayoutManager);

        ivList.add(R.drawable.ic_baseline_supervisor_account_24);
        ivList.add(R.drawable.ic_baseline_supervisor_account_24);
        ivList.add(R.drawable.ic_mk);
        txtData.add("DATA MAHASISWA");
        txtData.add("DATA DOSEN");
        txtData.add("DATA MATAKULIAH");

        rvDasboard.setAdapter(new DasboardAdapter(ivList,txtData));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_dasboard, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //Confirmation Dialog
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("KELUAR")
                .setMessage("Yakin ingin Logout ? ")
                .setPositiveButton("LOGOUT", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences.Editor editor = session.edit();
                        editor.clear();
                        editor.apply();
                        finish();
                        Toast.makeText(DasboardUTSActivity.this, "Logout Berhasil", Toast.LENGTH_SHORT).show();
                        Intent Intent = new Intent(DasboardUTSActivity.this, MainUTSActivity.class);
                        startActivity(Intent);
                    }
                })
                .setNegativeButton("BATAL", null)
                .setCancelable(false)
                .show();
        return true;
    }
}