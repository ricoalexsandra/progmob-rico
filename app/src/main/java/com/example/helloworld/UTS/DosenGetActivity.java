package com.example.helloworld.UTS;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helloworld.Adapter.DosenRecyclerAdapter;
import com.example.helloworld.Model.Dosen;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DosenGetActivity extends AppCompatActivity {

    RecyclerView rvDsn;
    DosenRecyclerAdapter dsnAdapter;
    ProgressDialog pd;
    List<Dosen> dosenList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_get);

        //Toolbar
        SharedPreferences pref = DosenGetActivity.this.getSharedPreferences("dosen_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        androidx.appcompat.widget.Toolbar mytoolbar = (Toolbar)findViewById(R.id.toolbarDosen);
        setSupportActionBar(mytoolbar);

        getSupportActionBar().setTitle("DATA DOSEN");

        rvDsn = (RecyclerView)findViewById(R.id.rvGetDsn);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Menunggu");
        pd.show();

        //Munculin di ItemView
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Dosen>> call = service.getDosen("72180198");

        call.enqueue(new Callback<List<Dosen>>() {
            @Override
            public void onResponse(Call<List<Dosen>> call, Response<List<Dosen>> response) {
                pd.dismiss();
                dosenList = response.body();
                dsnAdapter = new DosenRecyclerAdapter(dosenList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DosenGetActivity.this);
                rvDsn.setLayoutManager(layoutManager);
                rvDsn.setAdapter(dsnAdapter);
            }

            @Override
            public void onFailure(Call<List<Dosen>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(DosenGetActivity.this, "Error", Toast.LENGTH_LONG);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.itemAdd:
                Intent IntentAddDosen = new Intent(DosenGetActivity.this, DosenAddActivity.class);
                startActivity(IntentAddDosen);
        }
        return true;
    }
}