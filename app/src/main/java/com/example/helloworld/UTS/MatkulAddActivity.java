package com.example.helloworld.UTS;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.helloworld.Model.DefaultResult;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulAddActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_add);

        //Toolbar
        SharedPreferences pref = MatkulAddActivity.this.getSharedPreferences("matkul_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        androidx.appcompat.widget.Toolbar mytoolbar = (Toolbar)findViewById(R.id.toolbarAddMk);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setTitle("TAMBAH DATA MATAKULIAH");
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        EditText addNama = (EditText)findViewById(R.id.addNamaMk);
        EditText addKode = (EditText)findViewById(R.id.addKodeMk);
        EditText addHari = (EditText)findViewById(R.id.addHariMk);
        EditText addSesi = (EditText)findViewById(R.id.addSesiMk);
        EditText addSks = (EditText)findViewById(R.id.addSksMk);
        Button btnSimpan = (Button)findViewById(R.id.btnSimpanMk);

        pd = new ProgressDialog(MatkulAddActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_matkul(
                        addNama.getText().toString(),
                        "72180198",
                        addKode.getText().toString(),
                        addHari.getText().toString(),
                        addSesi.getText().toString(),
                        addSks.getText().toString()
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddActivity.this, "Data berhasil di inputkan", Toast.LENGTH_LONG).show();
                        Intent go = new Intent(MatkulAddActivity.this, MatkulGetActivity.class);
                        startActivity(go);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddActivity.this, "Data tidak berhasil di input", Toast.LENGTH_LONG).show();
                        Intent go = new Intent(MatkulAddActivity.this, MatkulGetActivity.class);
                        startActivity(go);
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        return true;
    }
}