package com.example.helloworld.UTS;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.helloworld.Model.User;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainUTSActivity extends AppCompatActivity {

    ProgressDialog pd;
    List<User> users;
    SharedPreferences session;
    private static int SPLASH_SCREEN = 3000;
    Animation top,bot;
    ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_u_t_s);

        EditText nimnik = (EditText)findViewById(R.id.nimnik);
        EditText pass = (EditText)findViewById(R.id.password);
        Button btnLogin = (Button)findViewById(R.id.btnLogin);
        pd = new ProgressDialog(MainUTSActivity.this);

        //Animation
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        logo = findViewById(R.id.imageView2);
        top = AnimationUtils.loadAnimation(this,R.anim.top_anim);
        bot = AnimationUtils.loadAnimation(this,R.anim.bott_anim);

        logo.setAnimation(top);
        nimnik.setAnimation(bot);
        pass.setAnimation(bot);

        //Login
        session = PreferenceManager.getDefaultSharedPreferences(MainUTSActivity.this);
        if(!session.getString("nimnik", "").isEmpty() && !session.getString("nama", "").isEmpty()) {
            finish();
            Intent goDasboard = new Intent(MainUTSActivity.this, DasboardUTSActivity.class);
            goDasboard.putExtra("nama", session.getString("nimnik",""));
            Bundle push = new Bundle();
            goDasboard.putExtras(push);
            startActivity(goDasboard);

            return;
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Loding..");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<User>> call = service.login(
                        nimnik.getText().toString(),
                        pass.getText().toString()
                );

                call.enqueue(new Callback<List<User>>() {
                    @Override
                    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                        pd.dismiss();
                        users = response.body();
                        if(users.size()==0){
                            pd.dismiss();
                            Toast.makeText(MainUTSActivity.this, "Username atau password salah.", Toast.LENGTH_SHORT).show();
                        }else{
                            User u = users.get(0);

                            SharedPreferences.Editor editor = session.edit();
                            editor.clear();
                            editor.putString("nimnik", u.getNimnik());
                            editor.putString("nama", u.getNama());
                            editor.apply();
                            finish();
                            Intent intent = new Intent(MainUTSActivity.this, DasboardUTSActivity.class);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<User>> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MainUTSActivity.this, "Username atau password salah.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}