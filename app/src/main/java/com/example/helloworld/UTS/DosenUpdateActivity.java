package com.example.helloworld.UTS;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.helloworld.Model.DefaultResult;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DosenUpdateActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_update);

        //Toolbar
        SharedPreferences pref = DosenUpdateActivity.this.getSharedPreferences("dosen_update_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        androidx.appcompat.widget.Toolbar mytoolbar = (Toolbar)findViewById(R.id.toolbarUpdateDosen);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setTitle("UPDATE DATA DOSEN");
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        EditText editNama = (EditText)findViewById(R.id.editNamaDosen);
        EditText editNidn = (EditText)findViewById(R.id.editNidnDosen);
        EditText editNidnCari = (EditText)findViewById(R.id.editNidnCari);
        EditText editAlamat = (EditText)findViewById(R.id.editAlamatDosen);
        EditText editEmail = (EditText)findViewById(R.id.editEmailDosen);
        EditText editGelar = (EditText)findViewById(R.id.editGelarDosen);
        Button btnEdit = (Button)findViewById(R.id.btnEdit);

        pd = new ProgressDialog(DosenUpdateActivity.this);

        Intent data = getIntent();
        editNama.setText(data.getStringExtra("nama"));
        editNidn.setText(data.getStringExtra("nidn"));
        editNidnCari.setText(data.getStringExtra("nidn"));
        editAlamat.setText(data.getStringExtra("alamat"));
        editEmail.setText(data.getStringExtra("email"));
        editGelar.setText(data.getStringExtra("gelar"));

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_dosen(
                        editNama.getText().toString(),
                        editNidn.getText().toString(),
                        editNidnCari.getText().toString(),
                        editAlamat.getText().toString(),
                        editEmail.getText().toString(),
                        editGelar.getText().toString(),
                        "auto",
                        "72180198"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this, "Data berhasil di Update", Toast.LENGTH_LONG).show();
                        Intent go = new Intent(DosenUpdateActivity.this, DosenGetActivity.class);
                        startActivity(go);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this, "Data tidak berhasil di Update", Toast.LENGTH_LONG).show();
                        Intent go = new Intent(DosenUpdateActivity.this, DosenGetActivity.class);
                        startActivity(go);
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        return true;
    }
}