package com.example.helloworld.UTS;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.helloworld.Model.DefaultResult;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DosenDeleteActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_delete);

        EditText delNidn = (EditText)findViewById(R.id.delNidn);
        Button btnDel;
        Button back = (Button)findViewById(R.id.btnDel);

        //Toolbar
        SharedPreferences pref = DosenDeleteActivity.this.getSharedPreferences("dosen_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        androidx.appcompat.widget.Toolbar mytoolbar = (Toolbar)findViewById(R.id.toolbarDelDosen);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setTitle("DELETE DATA DOSEN");
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Confirmation Dialog
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("HAPUS DATA")
                .setMessage("Yakin ingin Dihapus? ")
                .setPositiveButton("HAPUS", null)
                .setNegativeButton("BATAL", null)
                .setCancelable(false)
                .show();

        btnDel = dialog.getButton(AlertDialog.BUTTON_POSITIVE);

        pd = new ProgressDialog(DosenDeleteActivity.this);

        Intent data = getIntent();
        delNidn.setText(data.getStringExtra("nidn"));

        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_dosen(
                        delNidn.getText().toString(),
                        "72180198"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenDeleteActivity.this, "Data berhasil diupdate", Toast.LENGTH_LONG).show();
                        Intent go = new Intent(DosenDeleteActivity.this, DosenGetActivity.class);
                        startActivity(go);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenDeleteActivity.this, "Data tidak berhasil di update", Toast.LENGTH_LONG).show();
                        Intent go = new Intent(DosenDeleteActivity.this, DosenGetActivity.class);
                        startActivity(go);
                    }
                });
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DosenDeleteActivity.this, "Data tidak di Hapus", Toast.LENGTH_LONG).show();
                Intent go = new Intent(DosenDeleteActivity.this, DosenGetActivity.class);
                startActivity(go);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_del, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        return true;
    }
}