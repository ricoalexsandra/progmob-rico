package com.example.helloworld.UTS;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.helloworld.Model.DefaultResult;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulUpdateActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_update);

        //Toolbar
        SharedPreferences pref = MatkulUpdateActivity.this.getSharedPreferences("matkul_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        androidx.appcompat.widget.Toolbar mytoolbar = (Toolbar)findViewById(R.id.toolbarUpdateMk);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setTitle("UPDATE DATA MATAKULIAH");
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        EditText editNama = (EditText)findViewById(R.id.editNamaMk);
        EditText editKode = (EditText)findViewById(R.id.editKodemk);
        EditText editKodeCari = (EditText)findViewById(R.id.editKodeCari);
        EditText editHari = (EditText)findViewById(R.id.editHariMk);
        EditText editSesi = (EditText)findViewById(R.id.editSesiMk);
        EditText editSks = (EditText)findViewById(R.id.editSksMk);
        Button btnEdit = (Button)findViewById(R.id.btnEditMk);
        pd = new ProgressDialog(MatkulUpdateActivity.this);

        Intent data = getIntent();
        editNama.setText(data.getStringExtra("nama"));
        editKode.setText(data.getStringExtra("kode"));
        editKodeCari.setText(data.getStringExtra("kode"));
        editHari.setText(data.getStringExtra("hari"));
        editSesi.setText(data.getStringExtra("sesi"));
        editSks.setText(data.getStringExtra("sks"));

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_matkul(
                        editNama.getText().toString(),
                        "72180198",
                        editKode.getText().toString(),
                        editHari.getText().toString(),
                        editSesi.getText().toString(),
                        editSks.getText().toString(),
                        editKodeCari.getText().toString()
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this, "Data berhasil di Update", Toast.LENGTH_LONG).show();
                        Intent go = new Intent(MatkulUpdateActivity.this, MatkulGetActivity.class);
                        startActivity(go);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this, "Data tidak berhasil di Update", Toast.LENGTH_LONG).show();
                        Intent go = new Intent(MatkulUpdateActivity.this, MatkulGetActivity.class);
                        startActivity(go);
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        return true;
    }
}